﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sum_Square_Difference
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void bntClick_Click(object sender, RoutedEventArgs e)
        {
            int sumOfSquare = 0, squareOfSum = 0, naturalNumber = 0, answer = 0;

            while(naturalNumber != 100)
            {
                naturalNumber++;

                sumOfSquare = sumOfSquare + (naturalNumber * naturalNumber);

                squareOfSum = squareOfSum + naturalNumber;               
            }

            squareOfSum = squareOfSum * squareOfSum;

            answer = squareOfSum - sumOfSquare;

            lblAnswer.Content = "Answer: " + answer;
        }
    }
}
